getLocation <- function(sourceAddr){
  
  require(rjson)  
  origin <- sourceAddr
  
  # build the URL
  baseUrl <- "http://maps.googleapis.com/maps/api/geocode/json?address="
  #parse out the spaces in the url codes
  origin <- gsub(" ", "+", origin)
  finalUrl <- paste(baseUrl
                    , "address=", origin
                    , sep = "")
  
  # get the JSON returned by Google and convert it to an R list
  url_string <- URLencode(finalUrl)
  route <- fromJSON(paste(readLines(url_string), collapse = ""))
  
  return(route)
}
