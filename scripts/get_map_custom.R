get_map <- function (location = c(lon = -95.3632715, lat = 29.7632836), 
          zoom = "auto", scale = "auto", maptype = c("terrain", "satellite", 
                                                     "roadmap", "hybrid", "toner", "watercolor"), messaging = FALSE, 
          urlonly = FALSE, filename = "ggmapTemp", crop = TRUE, color = c("color", 
                                                                          "bw"), source = c("google", "osm", "stamen", "cloudmade"), 
          api_key,sizein=c(640,640)
                    ) 
{
  args <- as.list(match.call(expand.dots = TRUE)[-1])
  if ("verbose" %in% names(args)) {
    .Deprecated(msg = "verbose argument deprecated, use messaging.")
    messaging <- eval(args$verbose)
  }
  if ("center" %in% names(args)) {
    .Deprecated(msg = "center argument deprecated, use location.")
    location <- eval(args$center)
  }
  source <- match.arg(source)
  color <- match.arg(color)
  if (missing(maptype)) {
    if (source != "cloudmade") {
      maptype <- "terrain"
    }
    else {
      maptype <- 1
    }
  }
  if (source == "stamen") {
    if (!(maptype %in% c("terrain", "watercolor", "toner"))) {
      stop("when using stamen maps, only terrain, watercolor, and toner maptypes available", 
           call. = FALSE)
    }
  }
  if (source == "google" & (maptype == "toner" || maptype == 
                              "watercolor")) {
    message(paste0("maptype = \"", maptype, "\" is only available with source = \"stamen\"."))
    message(paste0("resetting to source = \"stamen\"..."))
    source <- "stamen"
  }
  location_stop <- TRUE
  if (is.character(location) && length(location) == 1) {
    location_type <- "address"
    location_stop <- FALSE
  }
  if (is.numeric(location) && length(location) == 2) {
    location_type <- "lonlat"
    location_stop <- FALSE
    if (!is.null(names(location))) {
      loc_names <- names(location)
      if (all(loc_names == c("long", "lat"))) {
        names(location) <- c("lon", "lat")
      }
      else if (all(loc_names == c("lat", "lon"))) {
        message("note : locations should be specified in the lon/lat format, not lat/lon.")
        location <- location[c("lon", "lat")]
      }
      else if (all(loc_names == c("lat", "long"))) {
        message("note : locations should be specified in the lon/lat format, not lat/lon.")
        location <- location[c("long", "lat")]
        names(location) <- c("lon", "lat")
      }
    }
    else {
      names(location) <- c("lon", "lat")
    }
  }
  if (is.numeric(location) && length(location) == 4) {
    location_type <- "bbox"
    location_stop <- FALSE
    if (length(names(location)) > 0) {
      if (!all(names(location) %in% c("left", "bottom", 
                                      "right", "top"))) {
        stop("bounding boxes should have name left, bottom, right, top)", 
             call. = FALSE)
      }
      location <- location[c("left", "bottom", "right", 
                             "top")]
    }
    else {
      names(location) <- c("left", "bottom", "right", "top")
    }
  }
  if (location_stop) {
    stop("improper location specification, see ?get_map.", 
         call. = F)
  }
  if (zoom == "auto" && location_type == "bbox") {
    if (zoom == "auto") {
      lon_range <- location[c("left", "right")]
      lat_range <- location[c("bottom", "top")]
      if (missing(zoom)) {        
        lonlength <- diff(lon_range)
        latlength <- diff(lat_range)
        zoomlon <- ceiling(log2(360 * 2/lonlength))
        zoomlat <- ceiling(log2(180 * 2/latlength))        
        zoom <- min(zoomlon, zoomlat) - 1
#         GLOBE_WIDTH = 256
#         angle = diff(lon_range)
#         if (angle < 0){
#           angle = angle + 360
#         }
#         message("Working out zoom!")
#         zoom = (round(log2(360 / angle / GLOBE_WIDTH)/log(2))) - 2
      }
    }
  }
  else if (zoom == "auto" && location_type != "bbox") {
    zoom = 10
  }
  if (scale == "auto") {
    if (source == "google") 
      scale <- 2
    if (source == "osm") 
      scale <- OSM_scale_lookup(zoom)
  }
  if (source == "google") {
    if (location_type == "bbox") {
      warning("bounding box given to google - spatial extent only approximate.", 
              call. = FALSE, immediate. = TRUE)
      message("converting bounding box to center/zoom specification. (experimental)")
      user_bbox <- location
      location <- c(lon = mean(location[c("left", "right")]), 
                    lat = mean(location[c("bottom", "top")]))
    }
    map <- get_googlemap(center = location, zoom = zoom, 
                         maptype = maptype, scale = scale, messaging = messaging, 
                         urlonly = urlonly, filename = filename, color = color, 
                         size=sizein)
    if (FALSE) {
      bb <- attr(map, "bb")
      mbbox <- c(left = bb$ll.lon, bottom = bb$ll.lat, 
                 right = bb$ur.lon, top = bb$ur.lat)
      size <- dim(map)
      if (location_type == "bbox") {
        slon <- seq(mbbox["left"], mbbox["right"], length.out = size[1])
        slat <- seq(mbbox["top"], mbbox["bottom"], length.out = size[2])
        keep_x_ndcs <- which(user_bbox["left"] <= slon & 
                               slon <= user_bbox["right"])
        keep_y_ndcs <- which(user_bbox["bottom"] <= slat & 
                               slat <= user_bbox["top"])
        map <- map[keep_y_ndcs, keep_x_ndcs]
        class(map) <- c("ggmap", "raster")
        attr(map, "bb") <- data.frame(ll.lat = user_bbox["bottom"], 
                                      ll.lon = user_bbox["left"], ur.lat = user_bbox["top"], 
                                      ur.lon = user_bbox["right"])
      }
    }
    return(map)
  }
  if (source == "osm") {
    if (location_type != "bbox") {
      gm <- get_googlemap(center = location, zoom = zoom, 
                          filename = filename)
      location <- as.numeric(attr(gm, "bb"))[c(2, 1, 4, 
                                               3)]
    }
    return(get_openstreetmap(bbox = location, scale = scale, 
                             messaging = messaging, urlonly = urlonly, filename = filename, 
                             color = color))
  }
  if (source == "stamen") {
    if (location_type != "bbox") {
      gm <- get_googlemap(center = location, zoom = zoom, 
                          filename = filename)
      location <- as.numeric(attr(gm, "bb"))[c(2, 1, 4, 
                                               3)]
    }
    return(get_stamenmap(bbox = location, zoom = zoom, maptype = maptype, 
                         crop = crop, messaging = messaging, urlonly = urlonly, 
                         filename = filename, color = color))
  }
  if (source == "cloudmade") {
    if (missing(api_key)) 
      stop("an api key must be provided for cloudmade maps, see ?get_cloudmademap.", 
           call. = FALSE)
    if (location_type != "bbox") {
      gm <- get_googlemap(center = location, zoom = zoom, 
                          filename = filename)
      location <- as.numeric(attr(gm, "bb"))[c(2, 1, 4, 
                                               3)]
    }
    return(get_cloudmademap(bbox = location, zoom = zoom, 
                            maptype = maptype, crop = crop, messaging = messaging, 
                            urlonly = urlonly, filename = filename, highres = TRUE, 
                            color = color, api_key = api_key))
  }
}