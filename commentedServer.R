if (!require("pacman")) install.packages("pacman")
pacman::p_load("shiny", "sp", "ggmap", "ggplot2")

require(shiny)
require(sp)
require(ggmap)
require(ggplot2)
library(data.table)

source('scripts/getLocation.R')
source('scripts/decodeLocation.R')  
source('scripts/get_map_custom.R')
source('scripts/getZoom.R')
source('scripts/getData.R')
source('scripts/forceLoad.R')



#Import data
data1 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2015_May.rds")
data2 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2015_June.rds")
data3 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2015_July.rds")
data4 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2015_August.rds")
data5 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2015_September.rds")
data6 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2015_October.rds")
data7 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2015_November.rds")
data8 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2015_December.rds")
data9 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2016_January.rds")
data10 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2016_February.rds")
data11 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2016_March.rds")
data12 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2016_April.rds")

#Combine data into list
dataList <- list(data1, data2, data3, data5, data6, data7, data8, data9, data10, data11, data12)

#Create colour palette for points
cbbPalette <- c("#808080", "#000000", "#FF0000", "#800000", "#FFFF00", "#808000", "#00FF00", "#008000", "#00FFFF", "#008080", "#0000FF", "#000080", "#FF00FF", "#800080")


# Define server logic required to generate and plot a random distribution
shinyServer(function(input, output) {
  
  # get the location
  locationReact <- reactive({
    
    require(ggmap)
    message("Starting server")    
    
    #check and see if we are getting the correct location:
    message(paste("Getting Location:", input$sourceLocation))
    
    #get the route in R format.
    location <- getLocation(input$sourceLocation)   
    
  })
  
  
  #get the location details if the location changes
  locationDetailsReact <- reactive({
    
    #get the location
    location <- locationReact()
    
    #decode to bouding box format
    locationDetails <- decodeLocation(location)
    
  })
  
  
  #get the location details if the zoom changes
  zoomDetailsReact <- reactive( {
    
    require(ggmap)
    zoom1 <- getZoom(input$zoom1)
    
  })
  
  
  #get the crime details if the months change         # May be cause of issue
  DataDetailsReact <- reactive( {
    
    require(ggmap)
    checkGroup <- getData(input$checkGroup)
    
  })
  
  
  #get the crime details if the months change         # May be cause of issue
  output$locationMap <- renderPlot({
    
    checkGroup <- DataDetailsReact()
    
    output$value <- renderPrint({ input$checkGroup }) 
    
    
    #if(2 %in% checkGroup)     ##### this almost does the job - but runs in the same issue as the intialising the data.crimes.
    #  {
    #   data.crime <- data2
    #  }
    
    
    locationDetails <- locationDetailsReact()
    zoom1 <- zoomDetailsReact()
    
    #draw a map
    map <- get_map(location = locationDetails$bbox, zoom = zoom1, source="google", maptype="roadmap")
    
    plt <- ggmap(map) +  
      geom_point(data=data.crime, aes(x=data.crime$Longitude, y=data.crime$Latitude, color=data.crime$Crime.type), size=3) +
      scale_colour_manual(values=cbbPalette)
    
    print(plt)
    
  })
  
   #Output table
    output$routeSummary <- renderTable({ 
    
    newDataList = list()
    
    checkGroup <- DataDetailsReact()  ## creates a vector with the numbers of boxes the user has selected.
    
   
    ##### Attempt 1 - this works, and changes the table. #####
    
    if (3 %in% checkGroup)   ##### if 3 - which is July, is selected, the data is switched
    {
      data.crime = data2      ##### to July, which is data2.
    }
    
    ##### End #####
    
    
    ##### Attempt 2 - this appears to work on its own, but not here (remove the comments): #####
    
    #    newDataList = list()     ## create an empty data list
    
    
    #    for (i in 1:length(checkGroup)){
    
    #      newDataList[i] = dataList[checkGroup[i]]
    
    
    #    }
    
    #    data.crime = rbindlist(newDataList)
    
    ##### End  #####
    
    
    locationDetails <- locationDetailsReact()
    
    
    #Gets data for summarys
    newdata <- subset(data.crime, (locationDetails$bbox["left"] < data.crime$Longitude) & (data.crime$Longitude < locationDetails$bbox["right"]) & 
                        (locationDetails$bbox["bottom"] < data.crime$Latitude) & (data.crime$Latitude< locationDetails$bbox["top"]))
    
    
    
    #return the table for rendering:

    crimes <-c("Anti-social behaviour", "Bicycle theft", "Burglary", "Criminal damage and arson", "Drugs", "Other theft", "Possession of weapons", "Public order","Robbery","Shoplifting", "Theft from the person", "Vehicle crime", "Violence and sexual offences Other crime" )
    
    #Counts number of crimes
    o <- rep("No Crime", 13)
    for (i in 1:13){
      o[i] = sum(newdata$Crime.type == crimes[i])
    }
    
    
    summaryTable <- cbind(Crime = crimes, Occurances = o) 
    
    
    output$Crimeplot <- renderPlot({
      
      plot2 <- qplot(newdata$Crime.type, fill=newdata$Crime.type) + scale_fill_manual(values=cbbPalette)
      
      print(plot2)
      
    })
    
    return(summaryTable) 
    

  })
  
  
})


