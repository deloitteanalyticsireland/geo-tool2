if (!require("pacman")) install.packages("pacman")
pacman::p_load("shiny", "sp", "ggmap", "ggplot2")

require(shiny)
require(sp)
require(ggmap)
require(ggplot2)

source('scripts/getLocation.R')
source('scripts/decodeLocation.R')  
source('scripts/get_map_custom.R')
source('scripts/getZoom.R')
source('scripts/getData.R')

data.shp = readRDS('C:/Users/gdzhus/Documents/geo-tool2/data/SA2011.rds')
data.crime = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2015_June.rds")
#c2 = readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2016_February.rds")
#data.crime <- rbind(c1, c2)



cbbPalette <- c("#808080", "#000000", "#FF0000", "#800000", "#FFFF00", "#808000", "#00FF00", "#008000", "#00FFFF", "#008080", "#0000FF", "#000080", "#FF00FF", "#800080")

# Define server logic required to generate and plot a random distribution
shinyServer(function(input, output) {
  
  # get the location
  locationReact <- reactive({
    
    require(ggmap)
    message("Starting server")    
    
    #check and see if we are getting the correct location:
    message(paste("Getting Location:", input$sourceLocation))
    
    #get the route in R format.
    location <- getLocation(input$sourceLocation)   
    
  })
  
  #get the location details if the location changes
  locationDetailsReact <- reactive({
    
    #get the location
    location <- locationReact()
    
    #decode to bouding box format
    locationDetails <- decodeLocation(location)
    
  })
  
  zoomDetailsReact <- reactive( {
    
    require(ggmap)
    zoom1 <- getZoom(input$zoom1)
    
  })
  
  DataDetailsReact <- reactive( {
    
    require(ggmap)
    checkGroup <- getData(input$checkGroup)
    
  })
  
  #checkGroup <- DataDetailsReact()
  #if('1' %in% checkGroup)
  #{
  #  data.crime <- readRDS("C:/Users/gdzhus/Documents/geo-tool2/data/2015_May.rds")
  # }
  
  output$locationMap <- renderPlot({
    
    locationDetails <- locationDetailsReact()
    zoom1 <- zoomDetailsReact()
    
    #draw a map
    map <- get_map(location = locationDetails$bbox, zoom = zoom1, source="google", maptype="roadmap")
    
    plt <- ggmap(map) +  
      geom_point(data=data.crime, aes(x=data.crime$Longitude, y=data.crime$Latitude, color=data.crime$Crime.type), size=3) +
      scale_colour_manual(values=cbbPalette)
    
    
    
    output$value <- renderPrint({ input$checkGroup })
    
    
    print(plt)
    
  })
  
  
  
  output$routeSummary <- renderTable({ 
    
    
    locationDetails <- locationDetailsReact()
    
    
    newdata <- subset(data.crime, (locationDetails$bbox["left"] < data.crime$Longitude) & (data.crime$Longitude < locationDetails$bbox["right"]) & 
                        (locationDetails$bbox["bottom"] < data.crime$Latitude) & (data.crime$Latitude< locationDetails$bbox["top"]))
    
    #return the table for rendering:
    #Add summary data
    
    crimes <-c("Anti-social behaviour", "Bicycle theft", "Burglary", "Criminal damage and arson", "Drugs", "Other theft", "Possession of weapons", "Public order","Robbery","Shoplifting", "Theft from the person", "Vehicle crime", "Violence and sexual offences Other crime" )
    
    o <- rep("No Crime", 13)
    for (i in 1:13){
      o[i] = sum(newdata$Crime.type == crimes[i])
    }
    
    summaryTable <- cbind(Crime = crimes, Occurances = o) 
    
    output$Crimeplot <- renderPlot({
      
      plot2 <- qplot(newdata$Crime.type, fill=newdata$Crime.type) + scale_fill_manual(values=cbbPalette)
      
      print(plot2)
      
    })
    
    return(summaryTable) 
    
    
    
    
    
    
    
  })
  
  
})


#Test
#location = getLocation("Bangor, Northern Ireland")
#locationCoords = decodeLocation(location)
#getCrashDetails(locationCoords)
